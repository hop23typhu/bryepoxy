<?php //template name: Trang chủ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php wp_head(); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/webservices/main.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/responsive.css"/>
  <script type="text/javascript" src="<?php echo THEME_URL; ?>/webservices/main.js" ></script>
  <script>
    $(window).scroll(function (){
      var topPage = $(window).scrollTop();
      if(topPage > 5) {
        $(".header").addClass("fixed");
       
        
      
      }else{
        $(".header").removeClass("fixed");
         
      }
      
    });
    </script>
</head>
<body <?php body_class(); ?> >      
<div id="wrapper">
    <div class="bg_header" id="divHeader">
        <div class="top">
            <div class="lang">
                <?php 
                    pll_the_languages( array(
                       'show_flags' => 1,
                       'dropdown' => 0,
                       'hide_if_empty'=>0,
                       'hide_current'=>0,
                       'show_names'=>1

                    ));
                 ?>
            </div>
            <div class="search">
                <form method="get" class="wse" action="<?php echo get_bloginfo( 'wpurl' ); ?>/">
                    <input 
                      name="" 
                      type="text"  
                      class="txtSearch"   
                      placeholder="<?php  echo get_faci_lang('Tìm kiếm'); ?>..." />
                    <input name="" type="submit" class="btnSearch" value='' />
                </form>
            </div><!--.search-->
            <div class="hotline">
                <span>
                  Hotline : 
                  <a  href="tel:<?php echo str_replace(' ', '', print_option('hotline'));  ?>" ><?php echo print_option('hotline'); ?></a>
                </span>
            </div><!-- hotline -->
        </div><!-- top -->
    </div><!-- bg_header -->
    <div id="container" class="container_12">
        <div class="column grid_12">
            <div class="header">
                
                <div class="menu">
                  <div class='logo'>
                      <a 
                          href="<?php bloginfo( 'wpurl' ); ?>" 
                           title='<?php bloginfo( 'name' ); ?>- <?php bloginfo( 'description' ); ?>'>
                         <img src="<?php echo print_option('site-logo'); ?>" alt="">
                      </a>
                      <h1> 
                          <?php
                              if(is_front_page() || is_home()){
                                  bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); 
                              }
                              else wp_title();
                          ?>
                      </h1>
                  </div>
                    <?php  
                            $args = array(
                                'theme_location' => 'primary-menu',
                                'menu' => '',
                                'container' => '',
                                'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                                'walker'          => new Description_Walker
                            );
                        
                            wp_nav_menu( $args );
                    ?>
                </div>
            </div><!-- .header -->
            <?php if(is_home() || is_front_page()){ ?>
            <div class="sliders">
               <?php echo do_shortcode( '[rev_slider alias="home-slide2"]' ); ?>
            </div><!-- .sliders -->
            <?php }?>
        </div>
        <!-- end .grid_12 -->
</div>
<?php if(!is_home() && !is_front_page()){ ?>
<div id="breadcrumb"><ul>
    <?php bcn_display(); ?>
</ul></div>
<?php }?>
<div  class="container_12">