<?php get_header(); ?>                   
<div class="column grid_9">
<h2 class="tit">
    <?php the_title(); ?>
</h2>
<?php 
    if(have_posts()):
?>
<ul class="lists">
    <div id="rptZone_ctl03_ctl00_divMessages"></div>
        <?php while(have_posts()):the_post(); ?>
        <li class="clearfix">
            <div class="img">
                <a href="<?php the_permalink();?>"  title="<?php the_title(); ?>">
                    <?php faci_post_image('large','',get_the_title()); ?>
                </a>
            </div>
            <div class="txt">
                <h3><a href="<?php the_permalink();?>"  title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                <p><?php the_excerpt();?></p>
                <p class="text-right"><a href="<?php the_permalink();?>" >Xem thêm ...</a></p>
            </div>
        </li>
        <?php endwhile; ?>
</ul>
<?php 
    wp_reset_postdata();
    echo do_shortcode( 'faci_breadcrumb' );
    else:  get_template_part('template-parts/content','none');
    endif;
?>    
</div><!-- end .grid_9 -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>