<?php get_header(); ?>                   
<div class="column grid_9"> 
<?php 
   while(have_posts()):the_post();
?>   
<div class="list-news" style="display:block;">
    <h1 id="rptZone_ctl03_ctl00_title" class="tit" style="display:block;"><?php the_title(); ?></h1>
</div>                   
<!--products-details-->
<div class="products-details">
    
  
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a id="351681" href="#dt351681">
                <?php  echo get_faci_lang('Đặc tính kỹ thuật'); ?></a></li>
        
            <li class=""><a id="1058532" href="#dt1058532">
                <?php  echo get_faci_lang('Quy trình thi công'); ?></a></li>
        
            <li class=""><a id="351682" href="#dt351682">
                <?php  echo get_faci_lang('Công trình đã ứng dụng'); ?></a></li>
                
        </ul>
        <div class="tab-content">
            
            <div class="tab-pane active item" id="dt351681">
                <?php the_content(); ?>
            </div>
        
            <div class="tab-pane item" id="dt1058532">
                <?php echo get_post_meta( get_the_id(),'service-procedure',true ); ?>
            </div>
        
            <div class="tab-pane" id="dt351682">
                <?php
                    $post_objects = get_field('service-project');
                    if( $post_objects ): 
                ?>
                <ul class="lists">
                <?php foreach( $post_objects as $post_object): ?>
                        <li class="clearfix">
                            <div class="img">
                                <a href="<?php echo get_permalink($post_object->ID); ?>" title="<?php echo get_the_title($post_object->ID); ?>">
                                <?php 
                                if(has_post_thumbnail( $post_object->ID ))
                                    echo get_the_post_thumbnail($post_object->ID,'large' ); 
                                else echo '<img src="'.print_option("image_notfound").'" alt="lỗi 404 ảnh" />';
                                ?>
                                </a>
                            </div>
                            <div class="txt">
                                <h3><a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a></h3>
                                <p><?php the_excerpt($post_object->ID); ?></p>
                                <p class="text-right"><a href="<?php echo get_permalink($post_object->ID); ?>"><?php  echo get_faci_lang('Xem thêm'); ?> ...</a></p>
                            </div>
                        </li>
                    <?php endforeach; ?>   
                </ul>
            <?php endif; ?>
         </div>
                
        </div>
        
	<script type="text/javascript">
            $(document).ready(function () {
                $('#myTab a').click(function (e) {
                    e.preventDefault();
                    $(this).tab('show');

                    $('.tab-pane').hide();
                    var id = '#dt' + $(this).attr('id');
                    $(id).show();
                })
            });
           
        </script>
    
</div>

<?php endwhile; ?>
</div><!-- end .grid_9 -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>