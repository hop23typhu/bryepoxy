            
</div>
<div class="column grid_12 " id="qfooter">
    <div class="dot"></div>
    <div class="sld">
        <div class="slider">
            <script type="text/javascript">
                jQuery(function () {
                    jQuery(".main .jCarouselLite").jCarouselLite({
                        speed: 1500,
                        vertical: false,
                        visible: 9,
                        auto: 1600,
                        width: 1060,
                        pauseOnHover: true
                    });
                });
            </script>
            <div class="carousel main">
                <div class="jCarouselLite">
                    <ul>
                        <?php 
                            $q_partner=new WP_Query(array('post_type'=>'portfolio','p'=>73));
                            if($q_partner->have_posts()):while($q_partner->have_posts()):$q_partner->the_post();
                         
                            $posts = get_field('g-gallery');
                            if( $posts ):foreach( $posts as $p ):
                        ?>
                            <li>
                                <a href="<?php echo $p['p-link']; ?>"  class="pic">
                                    <img 
                                        src="<?php echo $p['p-image']; ?>"  
                                        alt="<?php echo $p['p-title']; ?>" 
                                        title="<?php echo  $p['p-title'];; ?>" />
                                </a>
                            </li>
                        <?php  
                            endforeach;
                            endif;
                            endwhile;
                            endif;
                        ?>       
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- .sld -->

    <div class="footer row divfooter">
        <?php dynamic_sidebar( 'footer-widgets' ); ?>
    </div>
</div>
<!-- end .grid_12 --> 
<div class="bg_footer" id="divFooter"> </div>
</div>   
<?php wp_footer(); ?>
</body>
</html>
