<div class="column grid_3">                    
<div class="menu_left">
    <h3><?php  echo get_faci_lang('SẢN PHẨM - DỊCH VỤ'); ?></h3>
    
        <ul>
            <?php
                $taxonomy = 'service-category';
                $args = array(
                    'orderby'           => 'menu_order', 
                    'order'             => 'asc',
                    'hide_empty'        => false,
                    'fields'            => 'all', 
                ); 
                $tax_terms = get_terms($taxonomy,$args);
                foreach ( $tax_terms as $tax_term ) {
            ?>
            <li class="has-sub"><a title="<?php echo esc_html( $tax_term->name ); ?>" href="<?php echo esc_url( get_term_link( $tax_term->term_id ) ); ?>"><?php echo esc_html( $tax_term->name ); ?></a>
                <ul>
                   <?php  
                    $query_item_in_cat=null;
                    $query_item_in_cat= new WP_Query(
                        array(
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'service-category',
                                    'terms'    => array($tax_term->term_id),
                                ),
                            ),
                            'post_type'=>array( 'service'),
                            'order' => 'ASC' 
                        )
                    );
                    while($query_item_in_cat->have_posts()):$query_item_in_cat->the_post();
                    ?>
                        <li>
                        <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                    <?php endwhile; wp_reset_postdata(); ?>
               
                </ul>
            </li>
        <?php } ?>
        </ul>
</div>
</div>
<!-- end .grid_3 -->