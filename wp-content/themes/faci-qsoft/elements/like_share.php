<section class="kun-social">
  <div class="button"><div class="fb-like" data-href="<?php the_permalink();?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div></div>
  <div class="button"><div class="fb-share-button" data-href="<?php the_permalink();?>" data-type="button_count"></div></div>
  <div class="button"><div class="g-plus" data-action="share" data-annotation="bubble"></div></div>
  <div class="button"><div class="g-plusone" data-size="medium"></div></div>
  <div class="clear"></div>
</section><!-- .kun-social -->