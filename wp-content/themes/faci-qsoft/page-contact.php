<?php //template name: Liên hệ ?>
<?php get_header(); ?>                   
<div class="column grid_8">                        
	<div class="contact-us">

	   <?php 
		   while(have_posts()):the_post();
		   	the_content();
		   endwhile;
	   ?>
	</div>

</div>
<!-- end .grid_8 -->
<div class="column grid_4">
	<div class="support">
	    <h2 id="rptZone_ctl02_ctl00_tit" class="title"><?php  echo get_faci_lang('NHÂN VIÊN TƯ VẤN'); ?></h2>
	    <ul>
	    <?php 
	    	$q_team=new WP_Query(
	    		array('post_type'=>'team','orderby'=>'menu_order')
	    	);
	    	if($q_team->have_posts()):while($q_team->have_posts()):$q_team->the_post();
	    ?>
            <li class="clearfix">
                <div class="img">
                    <?php the_post_thumbnail( 'large' ); ?>
                </div>
                <div class="txt">
                  <ul>
                    <li class="name"><?php the_title(); ?></li>
                    <li class="mobile"><?php echo  get_post_meta( get_the_id(),'team-phone',true ); ?></li>
                    <li class="br"><a href="skype:<?php echo  get_post_meta( get_the_id(),'team-skype',true ); ?>?chat"><?php echo  get_post_meta( get_the_id(),'team-skype',true ); ?></a></li>
                    <li class="mail"><a href="mailto:<?php echo  get_post_meta( get_the_id(),'team-email',true ); ?>"><?php echo  get_post_meta( get_the_id(),'team-email',true ); ?></a></li>
                  </ul>
                </div>
            </li>
            <?php endwhile;wp_reset_postdata(); endif; ?>
	            
	    </ul>
	</div>

</div>
<!-- end .grid_4 -->
<?php get_footer(); ?>