<?php get_header(); ?>                   
<div class="column grid_9">                          
	<div class="news-details">
	<?php 
	   while(have_posts()):the_post();
	?> 
	    <h1 id="rptZone_ctl03_ctl00_ltrTitle"><?php the_title(); ?></h1>

	    <div id="rptZone_ctl03_ctl00_itemDetails">
	    	<?php include(get_template_directory().'/elements/like_share.php' ); ?>
	    	<div style="clear:both"><?php the_content(); ?></div>
	    	<?php include(get_template_directory().'/elements/like_share.php' ); ?>
	    </div>
	<?php endwhile; ?>    
	</div><!-- .news-details -->
	<div class="related-news" style="clear:both">
	    <h2 class="tit"><?php  echo get_faci_lang('Tin liên quan'); ?></h2>
	    <span id="rptZone_ctl03_ctl00_RelatedArticles_NotFoundData"></span>
		<ul class="related-items">
			<?php 
				//bài viết liên quan
				$id = get_the_id();
		    	$terms = get_the_terms( $id, 'category' );
		    	$arrid=array();
		    	foreach($terms as $term) {
			        $arrid=$term->term_id;   
			    }
				$args_q =	array(
						'orderby'=>'ID',
						'order'=>'DESC',
						'posts_per_page'=>5,
						'post__not_in'=>array($id),
						'tax_query' => array(
							array(
								'taxonomy' => 'category',
								'terms' => $arrid
							)
						)
				);
				$rela_post=new WP_Query($args_q); 
			?>
			<?php if($rela_post->have_posts()):while($rela_post->have_posts()):$rela_post->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>" >
					<?php the_title(); ?>
				</a>
			</li>
			<?php endwhile;wp_reset_postdata();  endif; ?>
			
		</ul>
	</div><!-- .related-news -->
</div>
<!-- end .grid_9 -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>