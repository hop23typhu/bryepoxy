<?php
/**
 * @package WordPress default theme
 * @subpackage Deeds theme of qsoft
 * @since deeds 1.0.3 (04/07/2016)
 * @author   Luong Ba Hop
 */
define('THEME_URL', get_template_directory_uri() );
define('THEME_DIR', get_template_directory() );
add_action( 'after_setup_theme', 'deeds_start_setup' );
if ( ! function_exists( 'deeds_start_setup' ) ) {
	function deeds_start_setup() {
		global $themename, $shortname, $options, $theme_path, $theme_uri;
		$themename  = 'deeds';
		$shortname  = 'deeds';
		load_theme_textdomain( 'deeds', get_template_directory() . '/languages' );
		$theme_path = get_template_directory();
		$theme_uri  = get_template_directory_uri();
		require_once( $theme_path . '/includes/install_plugins.php' );//install plugins theme need
		require_once( $theme_path . '/d_options/d-display.php' );//display option 
		require_once( $theme_path . '/d_options/d-custom.php' );
		//require_once( $theme_path . '/d_options/inc/mailchimp/d-mailchimp.php' );
		require_once( $theme_path . '/includes/functions_mail.php' );//process about mail
		require_once( $theme_path . '/includes/functions_frontend.php' );
		require_once( $theme_path . '/includes/functions_init.php' );
		require_once( $theme_path . '/includes/functions_styles.php' );//print option  Typography on head tag
		require_once( $theme_path . '/includes/functions_shortcodes.php' );// make shortcode for website . All shortcode here
		require_once( $theme_path . '/includes/function_ajax.php' );//process ajax 
		require_once( $theme_path . '/includes/custom-header.php' );
		require_once( $theme_path . '/includes/template-tags.php' );
		require_once( $theme_path . '/includes/extras.php' );
		require_once( $theme_path . '/includes/jetpack.php' );
		require_once( $theme_path . '/LIB/umanit-update-urls/updateurls.php' );
		require_once( $theme_path . '/LIB/vietnamese-slug/vietnamese_slug.php' );

		/** Create navigation **/
		if ( function_exists( 'wp_nav_menu') ) {
			add_theme_support( 'nav-menus' );
			register_nav_menus( array( 'primary-menu' => __( 'Primary Menu', $themename ) ) );
			register_nav_menus( array( 'cat-menu' => __( 'Sidebar Menu', $themename ) ) );
			register_nav_menus( array( 'bot-menu' => __( 'Bottom Menu', $themename ) ) );
		}
		/** Create sidebar **/
		register_sidebar( array(
			'name' => __( 'Sidebar widgets', $themename ),
			'id' => 'sidebar-widgets',
			'description' => __( 'Sidebar widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer widgets', $themename ),
			'id' => 'footer-widgets',
			'description' => __( 'Footer widgets', $themename ),
			'before_widget' => '<div  class="ftname col-xs-12  col-sm-6  col-md-4 ">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="name">',
			'after_title' => '</h2>',
		) );
		
		
		//add_theme_support( 'custom-logo' );//support LOGO
		add_theme_support( 'automatic-feed-links' );//make feed link in head
		add_theme_support( 'title-tag' );//make auto title tag
		add_theme_support( 'html5', array(//support html5 tags 
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'post-formats', array(//support some post format in post
			'video',
		) );
		add_theme_support( 'post-thumbnails' );//ddd post thumbnail
		//custom your template
		add_theme_support( 'custom-background', apply_filters( 'deeds_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
	}
}
//unregister some widgets
function remove_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Links');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Search');
     //unregister_widget('WP_Widget_Text');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
}
add_action('widgets_init', 'remove_default_widgets', 11);
//Prevent hack SQL Injection 
global $user_ID; 
if($user_ID) {
	if(!current_user_can('administrator')) {
        if (strlen($_SERVER['REQUEST_URI']) > 255 ||
            stripos($_SERVER['REQUEST_URI'], "eval(") ||
            stripos($_SERVER['REQUEST_URI'], "CONCAT") ||
            stripos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
            stripos($_SERVER['REQUEST_URI'], "base64"))
            {
                    @header("HTTP/1.1 414 Request-URI Too Long");
                    @header("Status: 414 Request-URI Too Long");
                    @header("Connection: Close");
                    @exit;
    		}
	}
}
//remove menubar (top) with user
if ( !current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}
//return limit 30 word for excerpt
function custom_exerpt_length($length){
	return 30;
}
add_filter('excerpt_length','custom_exerpt_length',999);
//change excerpt read more
function new_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
//info about me
function faci_dashboard() { ?>
	<h3>Website <?php echo bloginfo( 'name' ); ?>.</h3>
	<p><strong>Info contact of author</strong><br>
	<strong>Web Developer</strong> :  <a target="_blank" href="https://www.facebook.com/steve.luong.5">Luong Ba Hop</a><br>
	<strong>Email</strong>  luonghop.lc@gmail.com<br>
	<strong>Cellphone</strong> : 01632.434.165<br>
	<strong>Website</strong> : <a target="_blank" href="http://teachyourself.vn/">Teachyourself.vn</a></p> 
<?php }
add_action('wp_dashboard_setup', 'faci_welcome');
function faci_welcome() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'About me', 'faci_dashboard');
}
//change footer backend wordpress
add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin () {
	echo 'Powered by Faci Theme | Designed by <MARQUEE behavior=alternate direction=left scrollAmount=3 width="3%"><font face=Webdings style="color:#fff"></font></MARQUEE><MARQUEE scrollAmount=1 direction=left width="3%" style="color:#fff">| | |</MARQUEE><span class="call"><a href="https://www.facebook.com/steve.luong.5">Luong Ba Hop</a></span><MARQUEE scrollAmount=1 direction=right width="3%" style="color:#fff">| | |</MARQUEE><MARQUEE behavior=alternate direction=right scrollAmount=3 width="3%"><font face=Webdings style="color:#fff"></font></MARQUEE> </p>';
}

// disable logo wordpress in backend
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}
// add target blank to visit site
add_action( 'admin_bar_menu', 'customize_my_wp_admin_bar', 80 );
function customize_my_wp_admin_bar( $wp_admin_bar ) {
    //Get a reference to the view-site node to modify.
    $node = $wp_admin_bar->get_node('view-site');
    //Change target
    $node->meta['target'] = '_blank';
    //Update Node.
    $wp_admin_bar->add_node($node);
}
function change_jquery_file()
{
	if( !is_admin()){
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("https://code.jquery.com/jquery-2.2.4.min.js"), false, '1.3.2');
		wp_enqueue_script('jquery');
	}
}
add_action( 'wp_enqueue_scripts','change_jquery_file' );
/**
 * Create HTML list of nav menu items.
 * Replacement for the native Walker, using the description.
 *
 * @see    http://wordpress.stackexchange.com/q/14037/
 * @author toscho, http://toscho.de
 */
class Description_Walker extends Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array|object $args    Additional strings. Actually always an 
                                     instance of stdClass. But this is WordPress.
     * @return void
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
    {
        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

        $class_names = join(
            ' '
        ,   apply_filters(
                'nav_menu_css_class'
            ,   array_filter( $classes ), $item
            )
        );

        ! empty ( $class_names )
            and $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= "<li id='menu-item-$item->ID' $class_names>";

        $attributes  = '';

        ! empty( $item->attr_title )
            and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        ! empty( $item->target )
            and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        ! empty( $item->xfn )
            and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        ! empty( $item->url )
            and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';

        // insert description for top level elements only
        // you may change this
        $description = ( ! empty ( $item->description ) and 0 == $depth )
            ? '<i class="' . esc_attr( $item->description ) . '"></i><br/>' : '';

        $title = apply_filters( 'the_title', $item->title, $item->ID );

        $item_output = $args->before
            . "<a $attributes>"
            . $args->link_before
            . $description
            . $title
            . '</a> '
            . $args->link_after
            . $args->after;

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el'
        ,   $item_output
        ,   $item
        ,   $depth
        ,   $args
        );
    }
}
function faci_post_image($size='large',$class='',$title)
{
	if(has_post_thumbnail())
		the_post_thumbnail($size,array('class'=>$class,'alt'=>$title));
	else echo '<img class="'.$class.'" src="'.print_option( "image_notfound" ).'" alt="không tìm thấy ảnh , 404"/>';
}
/*
	**************END***************
*/