<?php //template name: Giới thiệu ?>
<?php get_header(); ?>                   
<div class="column grid_9"> 
<?php 
   while(have_posts()):the_post();
?>                      
<!--news details-->
<div class="news-details2">
    <h1 id="rptZone_ctl03_ctl00_title"><?php the_title(); ?></h1>
    <div class="details2">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a id="351681" href="#dt351681">
                <?php  echo get_faci_lang('Giới thiệu công ty'); ?></a></li>
        
            <li class=""><a id="1058532" href="#dt1058532">
                <?php  echo get_faci_lang('Quy trình làm việc'); ?></a></li>
        
            <li class=""><a id="351682" href="#dt351682">
                <?php  echo get_faci_lang('Đối tác nói về chúng tôi'); ?></a></li>
                
        </ul>
        <div class="tab-content">
            
                    <div class="tab-pane active" id="dt351681">
                        <?php the_content(); ?>
                    </div>
                
                    <div class="tab-pane " id="dt1058532">
                        <?php echo get_post_meta( get_the_id(),'about-role',true ); ?>
                    </div>
                
                    <div class="tab-pane " id="dt351682">
                        <?php echo get_post_meta( get_the_id(),'about-partner',true ); ?>
                    </div>
                
        </div>
        
	<script type="text/javascript">
            $(document).ready(function () {
                $('#myTab a').click(function (e) {
                    e.preventDefault();
                    $(this).tab('show');

                    $('.tab-pane').hide();
                    var id = '#dt' + $(this).attr('id');
                    $(id).show();
                })
            });
           
        </script>
    </div>
</div>

<?php endwhile; ?>
</div><!-- end .grid_9 -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>