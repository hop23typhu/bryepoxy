<?php  
/*
 *Hiển thị thêm cột trong trang danh sách ADMIN
*/


// Add to admin_init function
add_filter('manage_edit-service_columns', 'add_new_service_columns');
function add_new_service_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = _x('Tiêu đề', 'column name');
    $new_columns['author'] = __('Author');
    $new_columns['tags'] = __('Tags');
    $new_columns['date'] = _x('Date', 'column name');
    $new_columns['p-stt'] = __('Thứ tự');
    return $new_columns;
}

  // Add to admin_init function
add_action('manage_service_posts_custom_column', 'manage_service_columns' ,10, 2);
 
function manage_service_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {

    case 'p-stt':
        echo get_post_meta($post->ID, 'p-stt',true);
        break;
    default:
        break;
    } // end switch
}   