<?php  

add_action('wp_ajax_faci_contact', 'faci_contact');
add_action('wp_ajax_nopriv_faci_contact', 'faci_contact');
function faci_contact() {
	/*----------VALIDATION----------*/
	//full name 
	if (isset($_POST['cname']) & $_POST['cname']!='') $cname = $_POST['cname']; else die(__("<p class='error alert alert-danger'>Bạn chưa nhập họ tên  !</p>","deeds"));
	//email
	if (isset($_POST['cemail']) & $_POST['cemail']!='') 
	{
		$cemail = $_POST['cemail']; 
		if(!is_email( $cemail )) die( __("<p class='error alert alert-danger'>E-mail không đúng định dạng !</p>","deeds") );
	}
	else die( __("<p class='error alert alert-danger'>Bạn chưa nhập E-mail !</p>","deeds") );

	//Message
		if (isset($_POST['ccontent']) & $_POST['ccontent']!='') $ccontent = $_POST['ccontent']; else die(__("<p class='error alert alert-danger'>Bạn chưa nhập nội dung liên hệ !</p>","deeds"));


	/*----------INSERT DATABASE----------*/
	$post = array(
	    'post_title'    => $cname,
	    'post_content'    => $ccontent,
	    'post_status'   => 'pending',
	    'post_type' => 'contact',
	);
	$ajax_contact_id = wp_insert_post($post);
    add_post_meta($ajax_contact_id, 'cemail', $_POST['cemail'], true);
    add_post_meta($ajax_contact_id, 'cphone', $_POST['cphone'], true);
    /*----------SEND MAIL----------*/
    $body="<p style='font-weight:100;color:#444'>Họ tên : $cname - Email : $cemail - SĐT : $_POST[cphone] </p><p style='font-weight:100;color:#444'>Nội dung : $ccontent</p>";
    if( faci_send_mail( array( $cemail , get_bloginfo('admin_email') ),"Thư liên hệ",$body) ) 
    die( '
    	<script type="text/javascript">
            $("#cform").hide("fast");
        </script>    
        <p class="alert alert-success">'.get_theme_mod("txt_csuccess").'</p>
    ');
	else
	die( '
    	<p class="alert alert-danger">'.get_theme_mod("txt_cerror").'</p>
    '); 
	
}