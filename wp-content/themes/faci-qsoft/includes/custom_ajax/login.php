<?php
add_action('wp_ajax_faci_login', 'faci_login');
add_action('wp_ajax_nopriv_faci_login', 'faci_login');
function faci_login() {
	$creds = array();
	$creds['user_login'] = $_POST['user_username'];
	$creds['user_password'] = $_POST['user_password'];
	if($_POST['user_remember']!='' && $_POST['user_remember']=='forever') 
		$creds['remember'] = true; 
	else $creds['remember'] = false;

	$user = wp_signon( $creds, false );
	if ( is_wp_error($user) )
		echo "<p class='error'>Thông tin tài khoản không chính xác</p> ";
	else if(get_user_meta( $user->ID, 'has_to_be_activated', true ) != false)
		echo "<p class='error'>Tài khoản chưa kích hoạt</p> ";
	else 
	{
		 if ( isset( $user->roles ) && is_array( $user->roles ) ) {
                //check for admins
                if ( in_array( 'administrator', $user->roles ) ) {
                        // redirect them to the default place
                	echo "<script>jQuery(location).attr('href', '".admin_url()."');</script>";
                } else {
                        echo "<script>jQuery(location).attr('href', '".home_url()."');</script>";
                }
        } else echo "<script>jQuery(location).attr('href', '".home_url()."');</script>";
	}//end if else
	die;
} 
add_action( 'after_setup_theme', 'faci_login' );
