<?php  
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/includes/custom_login/logo.png);
            padding-bottom: 30px;
            background-size: 300px;
            width: 300px;
            height: 250px;
            pointer-events: none;
   			cursor: default;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_admin_logo() { ?>
    <style type="text/css">
        .customize-control-site_icon,
        #accordion-section-colors,
        #accordion-section-background_image,
        #toplevel_page_qsoft_settings_page,
        #toplevel_page_vc-general,
        #toplevel_page_edit-post_type-acf,
        .acf-column-2,
        #toplevel_page_WP-Optimize,
        #toplevel_page_wps_overview_page{
            <?php if(print_option('yn_hide_menu')=='yes'){ echo 'display: none !important;'; }?> 
        }
        #wpfooter{
        	background-color: #E14D40;
        	color: #fff;
            margin-bottom: 0px;
            padding-bottom: 0px !important;
        }
        #wpfooter a,#wpfooter p{
       	 color: #fff;
         line-height: 140% !important;
    	}
    	#wpfooter a{
    	text-decoration: underline !important; 
    }
    </style>
<?php }
add_action( 'admin_enqueue_scripts', 'my_admin_logo' );

function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/includes/custom_login/admin-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );