<?php
global $meta_boxes;
$meta_boxes = array();

$prefix = $shortname.'_';
/** Custom field for contact **/
$meta_boxes[] = array(
	'id'      => 'contact_meta_field',
	'title'   => 'Contact info',
	'pages'   => array( 'contact' ),
	'context' => 'side',//position
	'fields'  => array(
		array(
			'name' => __('E-mail', $themename),
			'id'   => "cemail",
			'type' => 'email',
			'desc' => __('Nhập địa chỉ E-mail ở đây .', $themename),
			'std'  => ''
		),
		array(
			'name' => __('Số điện thoại', $themename),
			'id'   => "cphone",
			'type' => 'text',
			'desc' => __('Nhập số điện thoại ở đây .', $themename),
			'std'  => ''
		)
	)
);




/** Metabox service link **/
/*$meta_boxes[] = array(
	'id'      => 'service_meta_link',
	'title'   => 'Service link',
	'pages'   => array( 'service' ),
	'context' => 'normal',
	'fields'  => array(
		array(
			'name' => __('Link caption', $themename),
			'id'   => "{$prefix}portfolio_meta_title",
			'type' => 'text',
			'desc' => __('Enter the caption for the button', $themename),
			'std'  => 'Visit site'
		),
		array(
			'name' => __('Link address', $themename),
			'id'   => "{$prefix}portfolio_meta_author",
			'type' => 'text',
			'desc'     => __('Enter the link address', $themename),
			'std' =>'http://'
		)
	)
);
*/
function deeds_register_meta_boxes() {
	global $meta_boxes;

	if ( class_exists( 'RW_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}
add_action( 'admin_init', 'deeds_register_meta_boxes' );
?>