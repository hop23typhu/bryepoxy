<?php
/*- Add currency -*/
add_theme_support( 'woocommerce' );
add_filter( 'woocommerce_currencies', 'add_currency_vnd' );
 
function add_currency_vnd( $currencies ) {
     $currencies['VND'] = __( 'Việt Nam Đồng', 'deeds' );
     return $currencies;
}
 
add_filter('woocommerce_currency_symbol', 'add_currency_vnd_symbol', 10, 2);
 
function add_currency_vnd_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'VND': $currency_symbol = ' <sup>đ</sup>'; break;
     }
     return $currency_symbol;
}

add_action( 'admin_enqueue_scripts', 'd_admin_script' );
function d_admin_script() {
	global $shortname;
	wp_enqueue_script('d-admin-scripts', get_template_directory_uri().'/d_options/js/d-admin.js', array(), '', true);
	wp_localize_script('d-admin-scripts', 'd_init_var', array(
		'd_shortname' => $shortname
	));
}
add_filter( 'add_menu_classes', 'show_pending_number_post');
function show_pending_number_post( $menu ) {
    $type = "post";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}
add_filter( 'add_menu_classes', 'show_pending_number_page');
function show_pending_number_page( $menu ) {
    $type = "page";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}
//require_once('custom_post/contact_type.php');

//require_once('custom_product/post_type.php');
//require_once('custom_product/my_field.php');
//add new post type
require_once('custom_post/services_type.php');
require_once('custom_post/project_type.php');
require_once('custom_post/portfolio_type.php');

//require_once('custom_post/testimonials_type.php');

require_once('custom_post/team_type.php');
//require_once('custom_post/clients_type.php');

//add new field
require_once('meta-box/meta-box.php');
require_once('functions_meta_box.php');

//add widget
//require_once('custom_widgets/lastest.php');
//require_once('custom_widgets/category.php');
//require_once('custom_widgets/social.php');


//custom form login admin
require_once('custom_login/login.php');


//customizer
//require_once( 'customizer/gerenal.php' );
//require_once( 'customizer/headers.php' );
//require_once( 'customizer/footers.php' );
//require_once( 'customizer/socials.php' );