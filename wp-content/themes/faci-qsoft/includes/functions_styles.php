<?php
function custom_wp_header() {
?>
	<base href="<?php bloginfo('wpurl'); ?>/">
	<meta property="fb:admins" content="<?php echo print_option('facebook-admin-id'); ?>"/>
	<meta property="fb:app_id" content="<?php echo print_option('facebook-api-key'); ?>" /> 
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if(print_option('site-favicon') != ''): ?>
		<link rel="shortcut icon" href="<?php echo print_option('site-favicon'); ?>" />
	<?php endif; ?>
	<link rel="stylesheet" href="<?php bloginfo('wpurl'); ?>/style.css">

	<!-- Custom styles & scripts-->
	<?php if(print_option('custom-style-code') != ''): ?>
	<style>
	<?php echo print_option('custom-style-code'); ?>
	</style>
	<?php endif; ?>

	<?php if(print_option('custom-script-code') != ''): ?>
	<script>
	<?php echo print_option('custom-script-code'); ?>
	</script>
	<?php endif; ?>

<?php
}
add_action('wp_head', 'custom_wp_header', 1);
function custom_wp_footer() {
	if(print_option("yn_gotop")=="yes"){
		echo do_shortcode( '[faci_back_to_top]' ); 
	}
?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$(window).scroll(function () {
				if ( $(this).scrollTop() > 500 )
					$("#totop").fadeIn();
				else
					$("#totop").fadeOut();
			});

			$("#totop").click(function () {
				$("body,html").animate({ scrollTop: 0 }, 800 );
				return false;
			});
		});
	</script>
	<script type="text/javascript">
	window.___gcfg = {lang: 'vi'};
	(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/platform.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
	</script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=<?php echo print_option('facebook-api-key'); ?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<?php 
		if(print_option('google-analytics-code') != ''): 
			echo print_option('google-analytics-code'); 
		endif; 
	?>
<?php
 }
add_action( 'wp_footer', 'custom_wp_footer' );
?>