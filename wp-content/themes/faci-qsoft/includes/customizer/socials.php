<?php


function socials_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'socials_session' , array(
	    'title'      => 'Faci social',
	    'priority'   => 32,
	) );
	//1 tuỳ chỉnh gồm 2 thành phần setting , và control
	

	$wp_customize->add_setting( 'appid_socials' , array(
	    'default'     => '1171840959527199'
	) );
	$wp_customize->add_control(
		'control_appid_socials', 
		array(
			'label'    => 'App ID',
			'section'  => 'socials_session',
			'settings' => 'appid_socials',
			'type'     => 'text',
		)
	);



	$wp_customize->add_setting( 'adminid_socials' , array(
	    'default'     => '9f72af4a100aeceb56beb5cff0b76384'
	) );
	$wp_customize->add_control(
		'control_adminid_socials', 
		array(
			'label'    => 'Admin ID',
			'section'  => 'socials_session',
			'settings' => 'adminid_socials',
			'type'     => 'text',
		)
	);


}
add_action( 'customize_register', 'socials_customize_register' );//thực thi hàm
