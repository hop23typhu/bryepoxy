<?php  
add_action( 'init', 'cptui_register_my_cpts_item' );
function cptui_register_my_cpts_item() {
	$labels = array(
		"name" => "Sản phẩm",
		"singular_name" => "Sản phẩm",
		"menu_name" => "Sản phẩm",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Sản phẩm",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite"   => array( "slug" => "san-pham", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-calendar-alt",		
		"supports" => array( "title", "thumbnail","editor" ),		
		"taxonomies" => array( "post_tag" ),	
		'menu_position'        =>20 ,	
	);
	register_post_type( "item", $args );

// End of cptui_register_my_cpts_item()
}


add_action( 'init', 'cptui_register_my_cpts_orders' );
function cptui_register_my_cpts_orders() {
	$labels = array(
		"name" => "Đơn hàng",
		"singular_name" => "Đơn hàng",
		"menu_name" => "Đơn hàng",
		);

	$args = array(
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite"   => array( "don-hang" => "thu-lien-he", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-cart",		
		"supports" => array( "title" ),	
		'menu_position'        =>20 ,			
	);
	register_post_type( "orders", $args );

// End of cptui_register_my_cpts_orders()
}




add_action( 'init', 'cptui_register_my_taxes_item_category' );
function cptui_register_my_taxes_item_category() {

	$labels = array(
		"name" => "Danh mục",
		"label" => "Danh mục",
		"menu_name" => "Danh mục",
		"all_items" => "Danh mục",
		);

	$args = array(
		"labels" => $labels,
		"hierarchical" => true,
		"label" => "Danh mục",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'danh-muc-san-pham', 'with_front' => true ),
		"show_admin_column" => false,
	);
	register_taxonomy( "item-category", array( "item" ), $args );

// End cptui_register_my_taxes_item_category()
}
add_filter( 'add_menu_classes', 'show_pending_number_item');
function show_pending_number_item( $menu ) {
    $type = "item";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}

add_filter( 'add_menu_classes', 'show_pending_number_orders');
function show_pending_number_orders( $menu ) {
    $type = "orders";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}
/* Product more info display */
add_filter('manage_edit-item_columns', 'add_new_item_columns');
function add_new_item_columns($columns) {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['thumb'] = __('Hình ảnh');
    $columns['title'] = _x('Title', 'column name');
    $columns['p-cat'] = __('Danh mục');
    $columns['p-price-sale'] = __('Giá bán');
    $columns['p-price'] = __('Giá gốc');
    return $columns;
}
// Add to admin_init function
add_action('manage_item_posts_custom_column', 'manage_item_columns' ,10, 2);
function manage_item_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {
    case 'thumb':
        if(has_post_thumbnail( $post->ID )) the_post_thumbnail( $post->ID ,array('large','style'=>'width:200px;height:auto;border:1px solid #ddd;padding:2px;') );
        break;
    case 'p-price':
        echo number_format(get_post_meta($post->ID,'p-price',true ));
        break;
     case 'p-price-sale':
        echo number_format(get_post_meta($post->ID,'p-price-sale',true ));
        break;
    case 'p-cat':
        $terms = get_the_terms( $post->ID, 'item-category' );
        foreach ($terms as $k => $v) {
            echo '<p><a href="'.esc_url( get_edit_term_link( $v->term_id, 'item-category' , 'portfolio' ) ).'">'.esc_html( $v->name ).'</a></p>';
        }
        
        break;
    
    default:
        break;
    } // end switch
} 