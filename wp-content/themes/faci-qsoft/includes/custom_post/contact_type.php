<?php
/* Contact register post type */
    
add_action('init', 'contact_register');   
function contact_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Thư liên hệ', 'post type general name', $themename),
        'singular_name'      => __('Thư liên hệ', 'post type singular name', $themename),
    );
    $args = array(  
        'labels'               => $labels,  
        'public'               => false,// hidden in front-end
        'show_ui'              => true,
        'show_in_menu'         => true,
        'show_in_nav_menus'    => false,
        'rewrite'              => false,
        'supports'             => array('title', 'editor'),
        'has_archive'          => true,
        'menu_icon'            => 'dashicons-email-alt',
        'exclude_from_search'  =>true,
        'menu_position'        =>20 ,
        "rewrite"              => array( "slug" => "thu-lien-he", "with_front" => true ),
       );   
  
    register_post_type( 'contact' , $args );  
}
/* Contact more info display */
add_filter('manage_edit-contact_columns', 'add_new_contact_columns');
function add_new_contact_columns($columns) {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['title'] = _x('Title', 'column name');
    $columns['cemail'] = __('E-mail');
    $columns['cphone'] = _('Số điện thoại');
    $columns['date'] = __('Date');
    
    return $columns;
}
// Add to admin_init function
add_action('manage_contact_posts_custom_column', 'manage_contact_columns' ,10, 2);
function manage_contact_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {
    case 'cemail':
        echo get_post_meta($post->ID, 'cemail',true);
        break;
     case 'cphone':
        echo get_post_meta($post->ID, 'cphone',true);
        break;
    default:
        break;
    } // end switch
} 
add_filter( 'add_menu_classes', 'show_pending_number_contact');
function show_pending_number_contact( $menu ) {
    $type = "contact";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}
