<?php
/*$args = array(
    "label"                         => "Nhóm danh mục", 
    "singular_label"                => "Nhóm danh mục", 
    'public'                        => true,
    'hierarchical'                  => true,
    'show_ui'                       => true,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),
    'rewrite'                       => false,
    'query_var'                     => true
);
register_taxonomy( 'portfolio-category', 'portfolio', $args ); */

add_action('init', 'portfolio_register');  
function portfolio_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Thư viện ảnh', 'post type general name', $themename),
        'singular_name'      => __('Thư viện ảnh', 'post type singular name', $themename),
    );
    $args = array(  
        'labels'            => $labels,  
        'public'            => false,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title','thumbnail'),
        'has_archive'       => true,
        'taxonomies'        => array(),
        'menu_icon'         => 'dashicons-format-gallery',
        'menu_position'        =>20 ,
        "rewrite"              => array( "slug" => "thu-vien-anh", "with_front" => true ),
       );  
    register_post_type( 'portfolio' , $args );  
}

/* Portfolio more info display */
add_filter('manage_edit-portfolio_columns', 'add_new_portfolio_columns');
function add_new_portfolio_columns($columns) {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['title'] = _x('Title', 'column name');
    $columns['cthumb'] = __('Hình ảnh');
    $columns['date'] = __('Ngày tạo');
    return $columns;
}
// Add to admin_init function
add_action('manage_portfolio_posts_custom_column', 'manage_portfolio_columns' ,10, 2);
function manage_portfolio_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {
    case 'cthumb':
        if(has_post_thumbnail( $post->ID )) the_post_thumbnail( $post->ID ,array('large','style'=>'width:250px;height:auto;border:1px solid #ddd;padding:2px;') );
        break;
   
    
    default:
        break;
    } // end switch
} 
add_filter( 'add_menu_classes', 'show_pending_number_portfolio');
function show_pending_number_portfolio( $menu ) {
    $type = "portfolio";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}