<?php
/* project 
$args = array(
    "label"                         => "Nhóm danh mục", 
    "singular_label"                => "Nhóm danh mục", 
    'public'                        => true,
    'hierarchical'                  => true,
    'show_ui'                       => true,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),
    'rewrite'                       => false,
    'query_var'                     => true
);
register_taxonomy( 'project-category', 'project', $args );*/
    


add_action('init', 'project_register');  
function project_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Dự án', 'post type general name', $themename),
        'singular_name'      => __('Dự án', 'post type singular name', $themename),
    );

    $args = array(  
        'labels'            => $labels,  
        'public'            => true,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'supports'          => array('title', 'editor', 'thumbnail','excerpt'),
        'has_archive'       => true,
        'menu_icon'         => 'dashicons-admin-appearance',
        'taxonomies'        => array('project-category'),
        "rewrite"              => array( "slug" => "du-an", "with_front" => true ),
        'menu_position'        =>20 ,
       );  
  
    register_post_type( 'project' , $args );  
}

/* project more info display */
add_filter('manage_edit-project_columns', 'add_new_project_columns');
function add_new_project_columns($columns) {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['title'] = _x('Title', 'column name');
    $columns['cthumb'] = __('Hình ảnh');
    $columns['date'] = __('Ngày tạo');
    return $columns;
}
// Add to admin_init function
add_action('manage_project_posts_custom_column', 'manage_project_columns' ,10, 2);
function manage_project_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {
    case 'cthumb':
        if(has_post_thumbnail( $post->ID )) the_post_thumbnail( $post->ID ,array('large','style'=>'width:250px;height:auto;border:1px solid #ddd;padding:2px;') );
        break;
   
    
    default:
        break;
    } // end switch
} 
add_filter( 'add_menu_classes', 'show_pending_number_project');
function show_pending_number_project( $menu ) {
    $type = "project";
    $status = "pending";
    $num_posts = wp_count_posts( $type, 'readable' );
    $pending_count = 0;
    if ( !empty($num_posts->$status) )
        $pending_count = $num_posts->$status;

    // build string to match in $menu array
    if ($type == 'post') {
        $menu_str = 'edit.php';
    } else {
        $menu_str = 'edit.php?post_type=' . $type;
    }

    // loop through $menu items, find match, add indicator
    foreach( $menu as $menu_key => $menu_data ) {
        if( $menu_str != $menu_data[2] )
            continue;
        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }
    return $menu;
}