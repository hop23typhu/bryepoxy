<?php  
$faci_lang=array(
	'Xem thêm'=>'Read more',
	'NHÂN VIÊN TƯ VẤN'=>'Guider',
	'Đặc tính kỹ thuật'=>'Specifications',
	'Quy trình thi công'=>'Construction process',
	'Công trình đã ứng dụng'=>'Application Projects',
	'Dự án'=>'Projects',
	'SẢN PHẨM - DỊCH VỤ'=>'PRODUCTS - SERVICES',
	'Quy trình làm việc'=>'Working process',
	'Giới thiệu công ty'=>'About the company',
	'Đối tác nói về chúng tôi'=>'Partners say about us',
	'Hôm nay'=>'Today',
	'Tổng lượt xem'=>'Total views',
	'Thống kê truy cập'=>'Statistical access',
	'Tìm kiếm'=>'Search',
	'Tin liên quan'=>'Related posts',
	'Từ khóa tìm kiếm'=>'Keyword',
	'Trang bạn đang tìm có thể đã được loại bỏ, hoặc tên của nó đã thay đổi, hoặc tạm thời không có. Hãy thử như sau:'=>'The page you are looking for might have been removed, or its name changed, or is temporarily unavailable. Try the following:',
	'Hãy chắc chắn rằng địa chỉ trang web hiển thị trong thanh địa chỉ của trình duyệt của bạn được viết và định dạng đúng.'=>'Make sure that the Web site address displayed in the address bar of your browser is written and formatted correctly.',
	'Quay lại'=>'Back',
	'trang chủ'=>'Home',
	'Chúng tôi không tìm thấy trang bạn yêu cầu'=>'We could not find the page you requested',
	'lỗi không tìm thấy trang'=>'
page not found error',
	
);