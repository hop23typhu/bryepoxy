<?php //template name: Tin tức ?>
<?php get_header(); ?>                   
<div class="column grid_9">
<div class="list-news" style="display:block !important">
<?php 
   while(have_posts()):the_post();
?>  
<h2 class="tit">
    <?php the_title(); ?>
</h2>
<?php 
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args_q =   array(
        'orderby'=>'menu_order',
        'paged' => $paged,
        'post__not_in'=>array($id),
        'post_type'=>array('post')
    );
    $news_post=new WP_Query($args_q); 
    if($news_post->have_posts()):
?>
<ul class="lists">
    <div id="rptZone_ctl03_ctl00_divMessages"></div>
        <?php while($news_post->have_posts()):$news_post->the_post(); ?>
        <li class="clearfix">
            <div class="img">
                <a href="<?php the_permalink();?>"  title="<?php the_title(); ?>">
                    <?php faci_post_image('large','',get_the_title()); ?>
                </a>
            </div>
            <div class="txt">
                <h3><a href="<?php the_permalink();?>"  title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                <p><?php the_excerpt();?></p>
                <p class="text-right"><a href="<?php the_permalink();?>" ><?php  echo get_faci_lang('Xem thêm'); ?> ...</a></p>
            </div>
        </li>
        <?php endwhile; ?>
</ul>
<?php 
    wp_pagenavi( ( array( 'query' => $news_post ) ) );
    wp_reset_query();
    else:  get_template_part('template-parts/content','none');
    endif;
?>
<?php endwhile; ?>   
</div>
</div><!-- end .grid_9 -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>