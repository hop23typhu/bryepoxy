<?php //template name: Dịch vụ ?>
<?php get_header(); ?>                   
<div class="column grid_9">
<?php 
   while(have_posts()):the_post();
?>  
<div class="list-products" >
    <h4 class="title" style="margin-right:40px;">
        <?php the_title(); ?>
    </h4>
    <div id="rptZone_ctl03_ctl00_divMessages" class="mess"></div>
    <ul>
    <?php
		$taxonomy = 'service-category';
		$args = array(
		    'orderby'           => 'menu_order', 
		    'order'             => 'asc',
		    'hide_empty'        => false,
		    'fields'            => 'all', 
		); 
		$tax_terms = get_terms($taxonomy,$args);
		foreach ( $tax_terms as $tax_term ) {
	?>
        <li class="page">
            <div class="image">
                <a href="<?php echo esc_url( get_term_link( $tax_term->term_id ) ); ?>" >
                    <img src="<?php the_field('services-image', $tax_term); ?>"  alt="<?php echo esc_html( $tax_term->name ); ?>" title="<?php echo esc_html( $tax_term->name ); ?>" /></a>
            </div>
            <div class="caption">
                <h2 class="ico1"><?php echo esc_html( $tax_term->name ); ?>
                    <a class="desc"><?php echo esc_html( $tax_term->description ); ?></a>
                </h2>
            </div>
        </li>
     <?php } ?>           
    </ul>
</div>
<div class="clearall"></div>
<?php endwhile; ?>    
</div><!-- end .grid_9 -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>