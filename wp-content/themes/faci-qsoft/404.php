<?php get_header(); ?>
<br/>
<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="text-center">
          	<small style="color:#fff"><?php  echo get_faci_lang('Chúng tôi không tìm thấy trang bạn yêu cầu'); ?>  - <b> <?php  echo get_faci_lang('lỗi không tìm thấy trang'); ?></b></small>
          </h3>
        </div>
        <div class="panel-body">
          <p><?php  echo get_faci_lang('Trang bạn đang tìm có thể đã được loại bỏ, hoặc tên của nó đã thay đổi, hoặc tạm thời không có. Hãy thử như sau:'); ?></p>
            <ul class="list-group col-md-6">
              <li class="list-group-item"><?php  echo get_faci_lang('Hãy chắc chắn rằng địa chỉ trang web hiển thị trong thanh địa chỉ của trình duyệt của bạn được viết và định dạng đúng.'); ?></li>

                <li class="list-group-item"><?php  echo get_faci_lang('Quay lại'); ?>   <a class="" href="<?php bloginfo('wpurl'); ?>"><?php  echo get_faci_lang('trang chủ'); ?></a> </li>
              </ul>
              <figure class="col-md-4">
              	<?php if(print_option("image_notfound")!=""){ ?><img class="img-responsive " src="<?php echo print_option( 'image_notfound' ); ?>" alt="không tìm thấy trang , 404"/> <?php } ?>
              </figure>
          </div>

        </div>
      </div>
      
    </div>
</div>
<?php get_footer(); ?>