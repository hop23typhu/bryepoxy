
<?php get_header(); ?>
        <div class="column grid_8">
            <div class="list-products">
                <h4 class="title">
                    SẢN PHẨM - DỊCH VỤ</h4>
                <div id="rptZone_ctl01_ctl00_divMessages" class="mess"></div>
                <ul>
                <?php
                    $taxonomy = 'service-category';
                    $args = array(
                        'orderby'           => 'menu_order', 
                        'order'             => 'asc',
                        'hide_empty'        => false,
                        'fields'            => 'all', 
                    ); 
                    $tax_terms = get_terms($taxonomy,$args);
                    foreach ( $tax_terms as $tax_term ) {
                ?>
                    <li>
                        <div class="image">
                            <a href="<?php echo esc_url( get_term_link( $tax_term->term_id ) ); ?>" >
                                <img src="<?php the_field('services-image', $tax_term); ?>"  alt="<?php echo esc_html( $tax_term->name ); ?>" title="<?php echo esc_html( $tax_term->name ); ?>" /></a>
                        </div>
                        <div class="caption">
                            <h2 class="ico1"><?php echo esc_html( $tax_term->name ); ?>
                                <a class="desc"><?php echo esc_html( $tax_term->description ); ?></a>
                            </h2>
                        </div>
                    </li>
                 <?php } ?>           
                </ul>
            </div><!-- .list-products -->
            <div class="clearall"></div>
            <div class="list-news">
                <div id="rptZone_ctl01_ctl00_divPager" class="pt"></div>
            </div>
        </div>
        <!-- end .grid_8 -->
        <div class="column grid_4">                       
            <div class="grid4_fix">
                <div class="video">
                    <div id="rptZone_ctl02_ctl00_showTitle">
                        <h2 class="title">
                            Video</h2>
                    </div>
                    <div class="vd">
                        <iframe width="300" height="214" src="https://www.youtube.com/embed/<?php echo print_option('youtube'); ?>"  frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="vd">
                        <div class="fb-like-box" data-href="<?php echo print_option('facebook-url'); ?>"  data-width="308" data-heigth="267" data-show-faces="true" data-stream="false" data-header="false"></div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- end .grid_4 -->
<?php get_footer(); ?>
        